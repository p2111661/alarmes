const time = document.getElementById('time');
const addAlarm = document.getElementById('addAlarm');

const nodeSectionAlarm = document.getElementById('alarms');
const audio = document.querySelector('audio');

const al = document.getElementById('exampleAlarm');

var alarmCounter = 1;

const btn_save = document.getElementById('saveAlarm');
const btn_load = document.getElementById('loadAlarm');

function updateTime(){
    let date = new Date();
    time.innerHTML=date.toLocaleTimeString();

    //A chaque 0 séconde on vérifies nos alarmes
    if(date.getSeconds() === 0){
        verifierAlarms();
    }
}
setInterval(updateTime,1000);


(function () {
     
    /**
     * On pourrait utiliser cloneNode() comme dans la fonction creteAlarm().
     * Mais je préfére pour cet exercice d'utiliser creteElement pour mieux
     * comprendre le fonctionnement.
     */
    addAlarm.addEventListener("click",function(){

        //section alarme
        var alarmSection = document.createElement('section');
        alarmSection.classList.add('alarm');
        alarmSection.setAttribute("id",alarmCounter);

        //input pour le temps
        var inputTime = document.createElement('input');
        inputTime.classList.add('time');
        inputTime.setAttribute("type","time");
        inputTime.setAttribute("title","alarm time "+alarmCounter);
        alarmSection.appendChild(inputTime);

        //input pour le titre
        var inputName = document.createElement('input');
        inputName.setAttribute("type","text");
        alarmSection.appendChild(inputName);

        //select pour le choix des sons
        var selectSound = document.createElement('select');
        selectSound.classList.add('chooseSound');

        let sound1 = document.createElement('option');
        sound1.setAttribute("label","sound 1");
        sound1.setAttribute("value","sound1.mp3");

        let sound2 = document.createElement('option');
        sound2.setAttribute("label","sound 2");
        sound2.setAttribute("value","sound2.mp3");

        let sound3 = document.createElement('option');
        sound3.setAttribute("label","sound 3");
        sound3.setAttribute("value","sound3.mp3");

        selectSound.appendChild(sound1);
        selectSound.appendChild(sound2);
        selectSound.appendChild(sound3);
        alarmSection.appendChild(selectSound);

        //bouton delete
        var buttonDelete = document.createElement('button');
        buttonDelete.setAttribute("id","deleteAlarm"+alarmCounter);
        buttonDelete.innerHTML = "-";
        alarmSection.appendChild(buttonDelete);

        //on ajoute la section au parent
        nodeSectionAlarm.appendChild(alarmSection);

        buttonDelete.addEventListener("click", () => {
            alarmSection.remove();
            
        })

        alarmCounter ++;

    });

})();


function verifierAlarms(){
    let date = new Date();
    //on récupère toutes les classes alarm
    let listAlarms = document.querySelectorAll('.alarm');
    for (let i = 1; i < listAlarms.length; i++) {
        //on récupère l'heure pour chaque alarm
        let hourMinute = listAlarms[i].querySelector('input[type="time"]').value;
        //on recupère le titre
        let title = listAlarms[i].querySelector('input[type="text"]').value
        //on recupère le sound choisi
        let soundChoosen = listAlarms[i].querySelector('select[class="chooseSound"]').value
        //on met l'heure et la minute dans tableau
        let hours = hourMinute.split(":",2)
        if (hours[0] == date.getHours() && hours[1] == date.getMinutes()){
            audio.src="sounds/"+soundChoosen;
            audio.play();
            console.log("L'alarme "+title+" sonne");
        }
        
    }
};


btn_save.addEventListener("click",() => {
    //on récupère toutes les classes alarm
    let listAlarms = document.querySelectorAll('.alarm');
    for (let i = 1; i < listAlarms.length; i++) {
        let hour = listAlarms[i].querySelector('input[type="time"]').value;
        //on vérifie si l'heure n'est pas nulle. si c'est pas nulle on sauvegarde l'alarm
        if(hour !== ""){
            //on sauvegarde l'heure, le titre et le son choisi.
            let alarme = [hour, listAlarms[i].querySelector('input[type="text"]').value, listAlarms[i].querySelector('select[class="chooseSound"]').value]
            let key = new String(i);
            localStorage.setItem(key.toString() ,alarme)
        }
    }


})


btn_load.addEventListener("click",() => {
    for (let i = 1; i <= localStorage.length; i++) {
        let key = new String(i);
        let alarmItem = localStorage.getItem(key.toString());
        //on sépare dans un tableau les différentes valeurs qu'on avait sauvegardé
        let alarmContent = alarmItem.split(",",3);
        
        createAlarm(key,alarmContent[0], alarmContent[1],alarmContent[2]);
    }
})


function createAlarm(key,heure,titre,sound){
    var alarme = al.cloneNode(true);

    alarme.querySelector('input[type="time"]').value = heure
    alarme.querySelector('input[type="text"]').value = titre
    alarme.querySelector('select[class="chooseSound"]').value = sound
    alarme.id = '';

    alarme.addEventListener("click",function(evt){
        //on vérifie le target de l'évènement pour supprimer l'alarme
        if(evt.target === alarme.querySelector('button')){
            localStorage.removeItem(key);
            alarme.remove();
        }
    })

    nodeSectionAlarm.appendChild(alarme)
}